package com.ciandt.delivery.test;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.ciandt.delivery.test.config.TestContext;
import com.ciandt.delivery.test.config.TestUtil;
import com.ciandt.delivery.test.config.WebAppContext;
import com.ciandt.deliverysystem.model.Rota;
import com.ciandt.deliverysystem.service.RotaService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebAppContext.class})
public class TestController {

	private MockMvc mockMvc;
	
    
    public SimpleMappingExceptionResolver exceptionResolver() {
        SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();

        Properties statusCodes = new Properties();

        statusCodes.put("error/404", "404");
        statusCodes.put("error/error", "500");

        exceptionResolver.setStatusCodes(statusCodes);

        return exceptionResolver;
    }
	 
    @Autowired
    private RotaService serviceMock;
	
	@Test
    public void getAllRotas() throws Exception {
		Rota firstR = new Rota("A","B",30);
		firstR.setId(1);
		Rota secondR = new Rota("B","C",100);
		secondR.setId(2);
		
		when(serviceMock.getAllRotas()).thenReturn(Arrays.asList(firstR, secondR));
		
        mockMvc.perform(get("/api/rotas"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.success", is(true)))

        .andExpect(jsonPath("$.data[0].id", is(1)))	
        .andExpect(jsonPath("$.data[0].origem", is("A")))
        .andExpect(jsonPath("$.data[0].destino", is("B")))
        .andExpect(jsonPath("$.data[0].distancia", is(30)))
        .andExpect(jsonPath("$.data[1].id", is(2)))	
        .andExpect(jsonPath("$.data[1].origem", is("B")))
        .andExpect(jsonPath("$.data[1].destino", is("C")))
        .andExpect(jsonPath("$.data[1].distancia", is(100)));
        
        verify(serviceMock, times(1)).getAllRotas();
        verifyNoMoreInteractions(serviceMock);
	}

}
