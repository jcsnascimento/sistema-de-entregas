package com.ciandt.delivery.test.config;

import javax.persistence.EntityManagerFactory;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.ciandt.deliverysystem.dao.RotaDAO;
import com.ciandt.deliverysystem.service.RotaService;

@Configuration
public class TestContext {
 
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
 
        messageSource.setBasename("i18n/messages");
        messageSource.setUseCodeAsDefaultMessage(true);
 
        return messageSource;
    }
 
    @Bean
    public RotaService rotaService() {
        return Mockito.mock(RotaService.class);
    }
    
    @Bean
    public RotaDAO dao() {
    	return Mockito.mock(RotaDAO.class);
    }
    
    @Bean
    public EntityManagerFactory emf() {
    	return Mockito.mock(EntityManagerFactory.class);
    }

    
}
