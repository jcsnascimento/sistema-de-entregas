package com.ciandt.deliverysystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Rota implements Serializable {

	private static final long serialVersionUID = -5074024498380011296L;

	@Id
	@GeneratedValue
	private long id;

	@Column
	private String origem;
	@Column
	private String destino;
	@Column
	private double distancia;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public Rota() {
		super(); 
	}

	public Rota(String inicio, String fim, double distancia ) {
		super();
		this.origem = inicio;
		this.destino = fim;
		this.distancia = distancia;
	}

}
