package com.ciandt.deliverysystem;

public class DeliveryRequest {

	private String origem;
	private String destino;
	private double autonomia;
	private double valor_do_litro;

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public double getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(double autonomia) {
		this.autonomia = autonomia;
	}

	public double getValor_do_litro() {
		return valor_do_litro;
	}

	public void setValor_do_litro(double valor_do_litro) {
		this.valor_do_litro = valor_do_litro;
	}

}
