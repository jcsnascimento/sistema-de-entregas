package com.ciandt.deliverysystem.dijkstra;

public class Edge {
	public final Vertex target;
	public final double distance;

	public Edge(Vertex argTarget, double adistance) {
		target = argTarget;
		distance = adistance;
	}

	public Vertex getTarget() {
		return target;
	}

	public double getDistance() {
		return distance;
	}
	
	
}
