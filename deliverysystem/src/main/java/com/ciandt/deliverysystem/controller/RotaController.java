package com.ciandt.deliverysystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciandt.deliverysystem.DeliveryRequest;
import com.ciandt.deliverysystem.DeliveryResponse;
import com.ciandt.deliverysystem.model.Rota;
import com.ciandt.deliverysystem.service.RotaService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Controller
public class RotaController {

	@Autowired
	private RotaService service;
	
	@ResponseBody
	@RequestMapping(value = "/api/rotas", method = RequestMethod.GET)
	public String rotas() {
		
		List<Rota> rotas =  service.getAllRotas();
		Gson gson = new Gson();
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("success", true);
		jsonObject.add("data", gson.toJsonTree(rotas));
		
		
		return gson.toJson(jsonObject);
	}
	
	@ResponseBody
	@RequestMapping(value = "/api/rota/add", method = RequestMethod.POST)
	public String addRota(@RequestBody Rota request) {
		Gson gson = new Gson();
		Rota r = service.save(request);
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("success", true);
		jsonObject.add("msg", gson.toJsonTree(r));
		
		return gson.toJson(jsonObject);
		
	}	
	
	@ResponseBody
	@RequestMapping(value = "/api/rota", method = RequestMethod.POST)
	public String distanciaMinima(@RequestBody DeliveryRequest request) {
		Gson gson = new Gson();
		DeliveryResponse response = service.mapa(request.getOrigem(), request.getDestino(), request.getAutonomia(), request.getValor_do_litro());
		return gson.toJson(response);
	}
		
}
