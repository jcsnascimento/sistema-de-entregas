package com.ciandt.deliverysystem.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ciandt.deliverysystem.model.Rota;

@Component
public class RotaDAO {
	
	@PersistenceContext
	private EntityManager em;


	public List<Rota> getAllRotas() {
		TypedQuery<Rota> query =  em.createQuery("SELECT r FROM Rota r ORDER BY r.origem", Rota.class);
		return query.getResultList();
	}
		
	
	@Transactional
	public Rota save(Rota rota) {
		Rota r = new Rota(rota.getOrigem(), rota.getDestino(), rota.getDistancia());
		em.persist(r);
		em.flush();
		return r;
	}
	
}
