package com.ciandt.deliverysystem.service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.deliverysystem.DeliveryResponse;
import com.ciandt.deliverysystem.dao.RotaDAO;
import com.ciandt.deliverysystem.dijkstra.Dijkstra;
import com.ciandt.deliverysystem.dijkstra.Vertex;
import com.ciandt.deliverysystem.dijkstra.Edge;
import com.ciandt.deliverysystem.model.Rota;

@Service
public class RotaService {

	@Autowired
	private RotaDAO dao;

	public List<Rota> getAllRotas() {
		return dao.getAllRotas();
	}

	public Rota save(Rota rota) {
		return dao.save(rota);
	}

	public DeliveryResponse mapa(String origem, String destino, double autonomia, double valor) {
		HashMap<String, Vertex> computedVertix = new HashMap<String, Vertex>();
		DeliveryResponse response = new DeliveryResponse();

		List<Rota> rotas = getAllRotas();
		for (Rota r : rotas) {
			Vertex orig = null;
			Vertex dest = null;

			if (!computedVertix.containsKey(r.getOrigem())) {
				orig = new Vertex(r.getOrigem());
				computedVertix.put(r.getOrigem(), orig);
			} else {
				orig = computedVertix.get(r.getOrigem());
			}

			if (!computedVertix.containsKey(r.getDestino())) {
				dest = new Vertex(r.getDestino());
				computedVertix.put(r.getDestino(), dest);
			} else {
				dest = computedVertix.get(r.getDestino());
			}

			orig.getAdjacencies().add(new Edge(dest, r.getDistancia()));
		}


		Vertex wantedOrigin = computedVertix.get(origem);
		Vertex wantedDestination = computedVertix.get(destino);

		Dijkstra.computePaths(wantedOrigin);
		List<Vertex> path = Dijkstra.getShortestPathTo(wantedDestination);

		response.setRota(path.stream().map(n -> String.valueOf(n)).collect(Collectors.joining(" ")));
		response.setCusto((wantedDestination.minDistance / autonomia) * valor);

		return response;
	}

}
