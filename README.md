# Sistema de Entregas #

Desenvolver um sistema de entregas visando encontrar o menor custo. Para popular sua base de dados o sistema precisa expor um API Rest que aceite o formato de malha logística (exemplo abaixo). É importante que os mapas sejam persistidos (Não precisa ser necessariamente um banco de dados). O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.

AB 10
BD 15
AC 20
CD 30
BE 50
DE 30

Exemplo de entrada:
{  
    "origem":"A",
    "destino":"D",
    "distancia":"30"
}

Expor uma nova API para o requisitante procurar o menor valor de entrega e seu caminho, para isso ele passará o nome do ponto de origem, nome do ponto de destino, autonomia do caminhão (km/l) e o valor do litro do combustível. 

Exemplo de entrada:
{  
    "origem":"A",
    "destino":"D",
    "autonomia":"10",
    "valor do litro":"2.50"
}

Resposta:
{  
    "rota":"A B D",
    "custo":"6.25"
}

** instalação **
mvn install package 
.war será gerado em \target\deliverysystem.war

